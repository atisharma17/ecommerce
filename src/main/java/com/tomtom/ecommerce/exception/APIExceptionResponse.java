package com.tomtom.ecommerce.exception;

import org.springframework.http.HttpStatus;

import java.time.Instant;

public class APIExceptionResponse {
    private HttpStatus status;
    private String message;
    private Instant timestamp;

    public APIExceptionResponse(HttpStatus status, String message, Instant timestamp) {
        this.status = status;
        this.message = message;
        this.timestamp = timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }
}
