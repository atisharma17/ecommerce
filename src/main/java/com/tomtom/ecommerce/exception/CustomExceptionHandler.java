package com.tomtom.ecommerce.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.UnexpectedTypeException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler {
    @ExceptionHandler({APIException.class})
    public ResponseEntity<APIExceptionResponse> handleException(APIException ex) {
        return new ResponseEntity<>(new APIExceptionResponse(ex.getStatus(), ex.getMessage(), Instant.now()), ex.getStatus());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<APIExceptionResponse> handleUnexpectedTypeException(MethodArgumentNotValidException ex) {
        List<String> errors = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            errors.add(error.getDefaultMessage());
        }
        return new ResponseEntity<>(new APIExceptionResponse(HttpStatus.BAD_REQUEST, errors.toString(), Instant.now()), HttpStatus.BAD_REQUEST);
    }
}
