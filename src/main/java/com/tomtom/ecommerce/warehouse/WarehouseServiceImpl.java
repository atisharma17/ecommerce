package com.tomtom.ecommerce.warehouse;

import com.tomtom.ecommerce.dto.Filters;
import com.tomtom.ecommerce.dto.Product;
import com.tomtom.ecommerce.dto.ProductRequest;
import com.tomtom.ecommerce.exception.APIException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
public class WarehouseServiceImpl implements WarehouseService {
    private int productCounter = 0;
    private List<Product> productList;

    private ReentrantLock lock1 = new ReentrantLock(true);

    public WarehouseServiceImpl() {
        this.productList = new ArrayList<>();
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        this.lock1.lock();
        try {
            Product product = new Product();
            product.setId(++productCounter);
            product.setCompany(productRequest.getCompany());
            product.setDescription(productRequest.getDescription());
            product.setName(productRequest.getName());
            product.setType(productRequest.getType());
            product.setPrice(productRequest.getPrice());
            this.productList.add(product);
            return productCounter;
        } finally {
            this.lock1.unlock();
        }
    }

    @Override
    public List<Product> getProducts(Filters filters) {
        List<Product> list = this.productList;
        if(filters == null) {
            return list;
        }
        if(filters.getId() != null) {
            list = list.stream().filter((a) -> a.getId().equals(filters.getId())).collect(Collectors.toList());
        }
        if(filters.getName() != null) {
            list = list.stream().filter((a) -> a.getName().equals(filters.getName())).collect(Collectors.toList());
        }
        if(filters.getCompany() != null) {
            list = list.stream().filter((a) -> a.getCompany().equals(filters.getCompany())).collect(Collectors.toList());
        }
        if(filters.getType() != null) {
            list = list.stream().filter((a) -> a.getType().equals(filters.getType())).collect(Collectors.toList());
        }
        if(filters.getMinPrice() != null) {
            list = list.stream().filter((a) -> a.getPrice() >= filters.getMinPrice()).collect(Collectors.toList());
        }
        if(filters.getMaxPrice() != null) {
            list = list.stream().filter((a) -> a.getPrice() <= filters.getMaxPrice()).collect(Collectors.toList());
        }
        return list;
    }

    @Override
    public Product getProductById(Integer prodId) {
        for(Product prod : this.productList) {
            if(prod.getId().equals(prodId)) {
                return prod;
            }
        }

        throw new APIException(HttpStatus.NOT_FOUND, "Product with ID: "+prodId+" is not available.");
    }
}
