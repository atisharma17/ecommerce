package com.tomtom.ecommerce.warehouse;

import com.tomtom.ecommerce.dto.Filters;
import com.tomtom.ecommerce.dto.Product;
import com.tomtom.ecommerce.dto.ProductRequest;

import java.util.List;

public interface WarehouseService {
    Integer addNewProduct(ProductRequest product);
    List<Product> getProducts(Filters filters);
    Product getProductById(Integer prodId);
}
