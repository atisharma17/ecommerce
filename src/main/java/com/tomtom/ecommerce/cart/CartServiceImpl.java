package com.tomtom.ecommerce.cart;

import com.tomtom.ecommerce.dto.Cart;
import com.tomtom.ecommerce.dto.Product;
import com.tomtom.ecommerce.warehouse.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CartServiceImpl implements CartService {

    private Map<String, Cart> mapOfUserAndCart = new HashMap<>();

    @Autowired
    WarehouseService warehouseService;
    
    @Override
    public void addItemsToCart(String userId, Integer prodId) {
        Cart prodsInCart = this.mapOfUserAndCart.get(userId);
        if(prodsInCart == null) {
            prodsInCart = new Cart();
        }
        prodsInCart.getAddedProducts().add(this.warehouseService.getProductById(prodId));
        updateTotalPrice(prodsInCart);
        this.mapOfUserAndCart.put(userId, prodsInCart);
    }

    private void updateTotalPrice(Cart prodsInCart) {
        Double totalPrice = 0.0;
        for(Product prod : prodsInCart.getAddedProducts()) {
            totalPrice += prod.getPrice();
        }
        prodsInCart.setTotalPrice(totalPrice);
    }

    @Override
    public void removeItemsFromCart(String userId, Integer prodId) {
        Cart prodsInCart = this.mapOfUserAndCart.get(userId);
        if(prodsInCart == null) {
            return;
        }
        prodsInCart.getAddedProducts().remove(warehouseService.getProductById(prodId));
        updateTotalPrice(prodsInCart);
        this.mapOfUserAndCart.put(userId, prodsInCart);
    }

    @Override
    public Cart getCartByUserId(String userId) {
        Cart cart = this.mapOfUserAndCart.get(userId);
        if(cart == null) {
            cart = new Cart();
        }
        return cart;
    }

    @Override
    public void clearCart(String userId) {
        this.mapOfUserAndCart.put(userId, new Cart());
    }
}
