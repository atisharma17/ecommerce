package com.tomtom.ecommerce.cart;

import com.tomtom.ecommerce.dto.Cart;

public interface CartService {
    void addItemsToCart(String userId, Integer prodId);
    void removeItemsFromCart(String userId, Integer prodId);
    Cart getCartByUserId(String userId);
    void clearCart(String userId);
}
