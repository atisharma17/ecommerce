package com.tomtom.ecommerce.dto;

import java.util.HashSet;
import java.util.Set;

public class Cart {
    private Set<Product> addedProducts;
    private Double totalPrice;

    public Cart() {
        this.addedProducts = new HashSet<>();
        this.totalPrice = 0.0;
    }

    public Set<Product> getAddedProducts() {
        return addedProducts;
    }

    public void setAddedProducts(Set<Product> addedProducts) {
        this.addedProducts = addedProducts;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
