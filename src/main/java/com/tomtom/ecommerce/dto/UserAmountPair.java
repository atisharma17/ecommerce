package com.tomtom.ecommerce.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UserAmountPair {
    @NotNull(message = "userId can not be null")
    @NotBlank(message = "userId can not be blank")
    private String userId;
    @NotNull(message = "amount can not be null")
    @Min(value = 0, message = "amount can not be negative")
    private Double amount;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
