package com.tomtom.ecommerce.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ProductRequest {
    @NotNull(message = "name field is required")
    @NotBlank(message = "name field can not be empty")
    private String name;
    @NotNull(message = "company field is required")
    @NotBlank(message = "company field can not be empty")
    private String company;
    @NotNull(message = "type field is required")
    @NotBlank(message = "type field can not be empty")
    private String type;
    private String description;
    @NotNull(message = "price field is required")
    @Min(value = 0, message = "price can not be negative")
    private Double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
