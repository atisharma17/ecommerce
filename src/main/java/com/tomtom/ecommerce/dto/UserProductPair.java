package com.tomtom.ecommerce.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UserProductPair {
    @NotNull(message = "userId can not be null")
    @NotBlank(message = "userId can not be blank")
    private String userId;
    @NotNull(message = "productId can not be null")
    private Integer productId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
}
