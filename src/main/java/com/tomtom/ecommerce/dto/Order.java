package com.tomtom.ecommerce.dto;

import java.time.Instant;

public class Order {
    private Integer orderId;
    private Instant time;
    private Cart itemsBought;

    public Order(Integer orderId, Instant time, Cart itemsBought) {
        this.orderId = orderId;
        this.time = time;
        this.itemsBought = itemsBought;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Cart getItemsBought() {
        return itemsBought;
    }

    public void setItemsBought(Cart itemsBought) {
        this.itemsBought = itemsBought;
    }
}
