package com.tomtom.ecommerce.controller.v1;

import com.tomtom.ecommerce.cart.CartService;
import com.tomtom.ecommerce.dto.*;
import com.tomtom.ecommerce.order.OrderService;
import com.tomtom.ecommerce.warehouse.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/ecommerce/v1")
public class ECommerceController {

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private CartService cartService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public List<Product> getProducts(@RequestBody @Nullable Filters filters) {
        return this.warehouseService.getProducts(filters);
    }

    @RequestMapping(value = "/new-product", method = RequestMethod.POST)
    public ResponseMessage addNewProduct(@Valid @RequestBody ProductRequest product) {
        Integer prodId = this.warehouseService.addNewProduct(product);
        String message = "New Product got added with Product Id: "+prodId;
        return new ResponseMessage("SUCCESS", message);
    }

    @RequestMapping(value = "/cart-item", method = RequestMethod.POST)
    public ResponseMessage addItemInCart(@Valid @RequestBody UserProductPair userProductPair) {
        this.cartService.addItemsToCart(userProductPair.getUserId(), userProductPair.getProductId());
        String message = "Product is added in your cart";
        return new ResponseMessage("SUCCESS", message);
    }

    @RequestMapping(value = "/cart-item", method = RequestMethod.DELETE)
    public ResponseMessage removeItemFromCart(@Valid @RequestBody UserProductPair userProductPair) {
        this.cartService.removeItemsFromCart(userProductPair.getUserId(), userProductPair.getProductId());
        String message = "Product is removed from your cart";
        return new ResponseMessage("SUCCESS", message);
    }

    @RequestMapping(value = "/cart-item/{userId}", method = RequestMethod.GET)
    public Cart getCartByUserId(@PathVariable String userId) {
        return this.cartService.getCartByUserId(userId);
    }

    @RequestMapping(value = "/order/{userId}", method = RequestMethod.GET)
    public List<Order> getOrderHistory(@PathVariable String userId) {
        return this.orderService.getOrderDetails(userId);
    }

    @RequestMapping(value = "/order/{userId}/{orderId}", method = RequestMethod.GET)
    public Order getOrderDetailsByOrderId(@PathVariable String userId, @PathVariable Integer orderId) {
        return this.orderService.getOrderDetailsById(userId, orderId);
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public Order placeOrder(@Valid @RequestBody UserAmountPair userAmountPair) {
        return this.orderService.placeOrder(userAmountPair.getUserId(), userAmountPair.getAmount());
    }
}
