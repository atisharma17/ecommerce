package com.tomtom.ecommerce.order;

import com.tomtom.ecommerce.cart.CartService;
import com.tomtom.ecommerce.dto.Cart;
import com.tomtom.ecommerce.dto.Order;
import com.tomtom.ecommerce.exception.APIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    private int orderIdCounter = 0;
        private Map<String, List<Order>> orderDetails = new HashMap<>();

    @Autowired
    private CartService cartService;

    @Override
    public Order placeOrder(String userId, Double amount) {
        Cart cart = this.cartService.getCartByUserId(userId);
        if(cart == null || cart.getAddedProducts() == null || cart.getAddedProducts().isEmpty()) {
            throw new APIException(HttpStatus.NOT_FOUND, "No Items available in Cart for User ID: "+userId);
        }
        if(!cart.getTotalPrice().equals(amount)) {
            throw new APIException(HttpStatus.BAD_REQUEST, "Please provide the exact payment of amount: "+cart.getTotalPrice());
        }
        Order order = new Order(++orderIdCounter, Instant.now(), cart);
        List<Order> orders = this.orderDetails.get(userId);
        if(orders == null) {
            orders = new ArrayList<>();
        }
        orders.add(order);
        this.orderDetails.put(userId, orders);
        this.cartService.clearCart(userId);
        return order;
    }

    @Override
    public List<Order> getOrderDetails(String userId) {
        List<Order> orders = this.orderDetails.get(userId);
        if(orders != null) {
            return orders;
        }
        throw new APIException(HttpStatus.NOT_FOUND, "No Order found for User ID: "+userId);
    }

    @Override
    public Order getOrderDetailsById(String userId, Integer orderId) {
        List<Order> orders = getOrderDetails(userId);

        for(Order order : orders) {
            if(order.getOrderId().equals(orderId)) {
                return order;
            }
        }

        throw new APIException(HttpStatus.NOT_FOUND, "No Order found with Order ID: "+orderId+" for User ID: "+userId);
    }
}
