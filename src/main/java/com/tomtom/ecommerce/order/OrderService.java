package com.tomtom.ecommerce.order;

import com.tomtom.ecommerce.dto.Order;

import java.util.List;

public interface OrderService {
    Order placeOrder(String userId, Double amount);
    List<Order> getOrderDetails(String userId);
    Order getOrderDetailsById(String userId, Integer orderId);
}
